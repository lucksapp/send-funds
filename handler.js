'use strict';
const {Nano} = require('nanode')
const nano = new Nano({url: 'http://172.31.95.149:7076'})

const cred = { privateKey: '5AAF44306DF02B2FEFDBD0387FF15D7ED1DFE605E640B128B24C5E833B0EAFD8',
publicKey: '81F26A1DB0B41428B50536079D9163E3B8F92F4B6AC3B1FE8DC2D4D7857A600C',
address: 'xrb_31hkfagu3f1n74ticfi9mpap9rxrz6qnptp5p9zauipnty4qnr1eowkd8bxk' };

module.exports.send = (event, context, callback) => {
      //let b = await nano.account(pk).info()
      //let b = await nano.account(pk).nanoBalance()
      //let b = await nano.key.create();
      //let b = await nano.available();
      //let b = await nano.rpc('key_create');
  void async function (){
    try {
      await nano.account(pk).receive()
    }
    catch (e) {
      // n é necessario tratar erros aqui
      // esse código apenas automatiza recebimento de fundos
      // pra nao ter que fazer manualmente
    }
    try {
      const balance = await nano.account(cred.privateKey).nanoBalance();

      if (balance <= event.amount){
        callback(null, {success: false, error: "insufficient"});
      } else {
        const send = await nano.account(cred.privateKey).send(event.amount, event.wallet);
        if (send){
          callback(null, {success: true});
        } else {
          callback(null, {success: false, error: "invalid"});
        }
      }
    }
    catch (e) {
      callback(null, {success: false, error: "unknown", log: e});
    }
  }();

};
